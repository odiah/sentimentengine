package com.nlp.sentiment.enhance.logic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nlp.sentiment.exception.LSIException;
import com.nlp.sentiment.exception.LSISentimentEnhancerException;
import com.nlp.sentiment.exception.SentimentEnhancerException;
import com.nlp.sentiment.lsi.LSI;
import com.sysomos.nlp.sentiment.Sentiment;

public class LSISentimentEnhancer extends SentimentEnhancer<String> {

	private static final float CUT_OFF = 0.6f;
	private final List<Sentiment> labelDocsSent;
	private static final Logger LOG = LoggerFactory
			.getLogger(LSISentimentEnhancer.class);

	/**
	 * Constructor
	 * 
	 * @param labeledDocs
	 *            List of documents with labels.
	 * @param labelDocsSent
	 *            Sentiment of each labeled document in labeldDocs
	 * @param unLabeledDocs
	 *            List of "neutral" or "none" documents
	 * @throws LSISentimentEnhancerException
	 */
	public LSISentimentEnhancer(final List<String> labeledDocs,
			final List<Sentiment> labelDocsSent,
			final List<String> unLabeledDocs) throws SentimentEnhancerException {
		super(labeledDocs, unLabeledDocs);
		if (CollectionUtils.isEmpty(labelDocsSent)) {
			throw new LSISentimentEnhancerException(
					"Labels of documents not specified. ");
		}
		this.labelDocsSent = labelDocsSent;
	}

	@Override
	public Map<String, Sentiment> label(final List<Sentiment> sentiments)
			throws LSISentimentEnhancerException {
		Map<String, Sentiment> sMap = new HashMap<String, Sentiment>();
		for (String doc : unLabeledDocs) {
			try {
				Map<Integer, Double> relevanceScores = rank(doc);
				sMap.put(doc, classify(relevanceScores));
			} catch (LSIException e) {
				LOG.error(e.getLocalizedMessage());
				sMap.put(doc, Sentiment.NONE);
			}
		}
		return sMap;
	}

	private Sentiment classify(final Map<Integer, Double> relevanceScores)
			throws LSISentimentEnhancerException {
		if (MapUtils.isEmpty(relevanceScores)) {
			throw new LSISentimentEnhancerException(
					"Relevance Scores are empty. ");
		}
		Map<Sentiment, Integer> sCount = new HashMap<Sentiment, Integer>();

		for (Integer docIndex : relevanceScores.keySet()) {
			Sentiment docSentiment = labelDocsSent.get(docIndex);
			if (docSentiment == Sentiment.POS) {
				if (!sCount.containsKey(docSentiment)) {
					sCount.put(docSentiment, 1);
				} else {
					int count = sCount.get(docSentiment);
					sCount.put(docSentiment, ++count);
				}
			}
		}

		for (Map.Entry<Sentiment, Integer> entry : sCount.entrySet()) {
			float ratio = (float) ((1.0 * entry.getValue()) / sCount.keySet()
					.size());
			if (ratio >= CUT_OFF) {
				return entry.getKey();
			}
		}

		return Sentiment.NEUTRAL;
	}

	private Map<Integer, Double> rank(String query) throws LSIException {

		if (StringUtils.isEmpty(query)) {
			return null;
		}
		LSI lsi = new LSI(labeledDocs);
		return lsi.queryDocsRelevance(query);
	}
}
