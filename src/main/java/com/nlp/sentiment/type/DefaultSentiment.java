package com.nlp.sentiment.type;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.nlp.sentiment.map.api.MapApiResponse;
import com.nlp.sentiment.map.api.analytics.SentimentInfo;

public abstract class DefaultSentiment {

	private String xmlContent = null;
	protected static final Logger LOG = LoggerFactory
			.getLogger(DefaultSentiment.class);

	public DefaultSentiment(String xmlContent) {
		this.xmlContent = xmlContent;
	}

	protected MapApiResponse getResponse() {
		if (StringUtils.isEmpty(xmlContent)) {
			return null;
		}
		try {
			JAXBContext jaxbContext = JAXBContext
					.newInstance(MapApiResponse.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

			StringReader reader = new StringReader(xmlContent);
			MapApiResponse response = (MapApiResponse) unmarshaller
					.unmarshal(reader);
			return response;
		} catch (JAXBException e) {
			e.printStackTrace();
			LOG.error(e.getLocalizedMessage(), e);
			return null;
		}
	}

	public abstract SentimentInfo getSentimentInfo();
}
