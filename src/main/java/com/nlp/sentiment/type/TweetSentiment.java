package com.nlp.sentiment.type;

import com.nlp.sentiment.map.api.MapApiResponse;
import com.nlp.sentiment.map.api.analytics.SentimentInfo;
import com.nlp.sentiment.map.api.results.TwitterResults;

public class TweetSentiment extends DefaultSentiment {

	public TweetSentiment(String xmlContent) {
		super(xmlContent);
	}

	@Override
	public SentimentInfo getSentimentInfo() {
		MapApiResponse response = getResponse();
		if (response == null) {
			return null;
		}
		TwitterResults results = response.getTwitterResults();
		return results.getAnalytics().getSentiment();
	}

}
