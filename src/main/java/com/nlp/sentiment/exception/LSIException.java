package com.nlp.sentiment.exception;

public class LSIException extends Exception {

	private static final long serialVersionUID = 6807413720039411255L;

	public LSIException(Throwable cause) {
		super(cause);
	}

	public LSIException(String message) {
		super(message);
	}

	public LSIException(Exception e) {
		super(e);
	}

}
