package com.nlp.sentiment.map.api.analytics;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "top_countries")
@XmlAccessorType(XmlAccessType.FIELD)
public class TopCountries {
	@XmlElement(name = "country")
	protected List<CountryInfo> countries;

	public List<CountryInfo> getCountries() {
		return countries;
	}

	public void setCountries(List<CountryInfo> countries) {
		this.countries = countries;
	}

}

