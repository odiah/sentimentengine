package com.nlp.sentiment.map.api.results;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.nlp.sentiment.map.api.analytics.Analytics;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(propOrder = { "request", "analytics" })
public class BasicResults {
	@XmlElement(name = "request")
	private String request;
	@XmlElement(name = "analytics")
	private Analytics analytics;

	public String getRequest() {
		return request;
	}

	public void setRequest(String request) {
		this.request = request;
	}

	public Analytics getAnalytics() {
		return analytics;
	}

	public void setAnalytics(Analytics analytics) {
		this.analytics = analytics;
	}
}
